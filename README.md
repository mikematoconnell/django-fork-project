# Django Fork Project

## Description
Easily let's me fork a Django project with all of the initial setup that I need. Includes profilers

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
If you have uv installed you can use init a project and run the application after installing the dependencies. If not you can create a virtual environment and then install the packages from the requirements.txt file.
