from functools import partial

from django.db.models import signals
from django.utils import timezone

from forkprojectapplication.common.models import CustomBaseModel
